package labcodeinspection;

import java.util.Locale;

public class Email {
	final private String mfirstName;
	final private String mlastName;
	private String password;
	private String department;
	final private int defPasswordLength = 8;
	private String email; 
	
	/**
	 * Constructor for Email class
	 * @param firstName  First name of user
	 * @param lastName   Last name of user
	 * */
	public Email(final String firstName, final String lastName) {
		this.mfirstName = firstName;
		this.mlastName = lastName;
	}

	/**
	 * Print information of user, email, and email credential
	 * */
	public void showInfo() {
		System.out.println("\nFIRST NAME= " + mfirstName + "\nLAST NAME= " + mlastName);
		System.out.println("DEPARMENT= " + department + "\nEMAIL= " + email + "\nPASSWORD= " + password);
	}
	
	/**
	 * Setter for department method
	 * @param depChoice Department code
	 * */
	public void setDeparment(final int depChoice) {
		switch (depChoice) {
		case 1:
			this.department = "sales";
			break;
		case 2:
			this.department = "dev";
			break;
		case 3:
			this.department = "acct";
			break;
		default:
			break;
		}
	}

	private String randomPassword(final int length) {
		final String set = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890#$&@*";
		final char[] password = new char[length];
		for (int i = 0; i < length; i++) {
			final int rand = (int) (Math.random() * set.length());
			password[i] = set.charAt(rand);
		}
		return new String(password);
	}
	
	/**
	 * Generate email address and set it to email attribute of this class
	 *  */
	public void generateEmail() {
		this.password = this.randomPassword(this.defPasswordLength);
		this.email = this.mfirstName.toLowerCase(Locale.ROOT) + this.mlastName.toLowerCase(Locale.ROOT) + "@" + this.department
				+ ".espol.edu.ec";
	}
}
